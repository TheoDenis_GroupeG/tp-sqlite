import unittest
import os, sqlite3
from sqlite import DB_Create, CheckExistance_DB

class Test_SQLite(unittest.TestCase):

    '''
    Disabled by new way of working of CheckExistance_DB(), that no longer takes arguments in.

    def test_bdd_existance(self):

        path = "tests/test.sqlite"
        self.assertEqual(True, CheckExistance_DB(path))

        path = "test.sqlite"
        self.assertEqual(False, CheckExistance_DB(path))

        path = "tests/test"
        self.assertEqual(False, CheckExistance_DB(path)) 
    '''

    def test_bdd_creation(self):

        self.assertEqual(False, CheckExistance_DB())
        DB_Create()
        self.assertEqual(True, CheckExistance_DB())
