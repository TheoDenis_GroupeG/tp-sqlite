import unittest
from sqlite import CheckExistance_csv

class Test_CSV(unittest.TestCase):

    def test_check_csv(self):
        
        self.assertEqual(False, CheckExistance_csv("tests/test", "csv"))
        self.assertEqual(False, CheckExistance_csv("tests/test_csv.py", "csv"))
        self.assertEqual(True, CheckExistance_csv("tests/test.csv", "csv"))

''' WIP
    def test_read_csv(self):

        path = "tests/test.csv"

        data = [
            {'Licence': "SW", 'BG-Color': "Red", 'Year': "1977"}
            {'Licence': "LOTR", 'BG-Color': "Green", 'Year': "1937"}
            {'Licence': "ASoIaF", 'BG-Color': "Blue", 'Year': "1996"}
            {'Licence': "JJBA", 'BG-Color': "Purple", 'Year': "1987"}
        ]
        
        self.assertEqual(data, read(path, '/'))

        data = [
            {'Licence/BG-Color/Year': "SW/Red/1977"},
            {'Licence/BG-Color/Year': "LOTR/Green/1937"},
            {'Licence/BG-Color/Year': "ASoIaF/Blue/1996"},
            {'Licence/BG-Color/Year': "JJBA/Purple/1987"}
        ]

        self.assertNotEqual(data, read(path, '/'))
'''
