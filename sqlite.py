import logging
import sqlite3
import csv
import sys
import os
import os.path
import unicodedata
from copy import copy
from datetime import datetime

caracteres_combinaison = dict.fromkeys(c for c in range(sys.maxunicode)
if unicodedata.combining(chr(c)))

log_path = ('./logs/') #logs path
db_path = ('./db.sqlite3') #DB path
csv_path = ('./data.csv') #csv input path

def error(error, connection): #Called if an error occured in DB

    connection.rollback()
    connection.close()
    logging.warning("ERROR: Resetting DB and closing connection" + error)

def clear(text): #Clears text before exporting into DB

    new = text.replace(";", "").replace("--", " ").replace('"', '').replace("drop ", " ").replace("DROP ", "")
    new = unicodedata.normalize('NFKD', new).encode('ascii', 'ignore')
    logging.debug("Transformed %s into %s", text, new)

    return new.decode("ascii")

def dicclear(row): #Creates and clears a dic
    logging.debug("Translating and clearing dic")
    export_dict = {
            'adresse_titulaire': clear(row['adresse_titulaire']),
            'nom': clear(row['nom']),
            'prenom':clear(row['prenom']),
            'immatriculation':clear(row['immatriculation']),
            'date_immatriculation':clear(row['date_immatriculation']) ,
            'vin':clear(row['vin']) ,
            'marque': clear(row['marque']) ,
            'denomination_commerciale': clear(row['denomination_commerciale']) ,
            'couleur': clear(row['couleur']) ,
            'carrosserie': clear(row['carrosserie']),
            'categorie': clear(row['categorie']) ,
            'cylindres': clear(row['cylindres']) ,
            'energie': clear(row['energie']) ,
            'places': clear(row['places']) ,
            'poids': clear(row['poids']) ,
            'puissance': clear(row['puissance']) ,
            'type': clear(row['type']) ,
            'variante': clear(row['variante']) ,
            'version': clear(row['version']) 
        }
    return export_dict  

def inddicclear(row, index): #Creates and clears an indexed dic
    logging.debug("Traduction et nettoyage des variable d'un dictionnaire")
    export_dict = {
            'adresse_titulaire': clear(row['adresse_titulaire']),
            'nom': clear(row['nom']),
            'prenom':clear(row['prenom']),
            'immatriculation':clear(row['immatriculation']),
            'date_immatriculation':clear(row['date_immatriculation']) ,
            'vin':clear(row['vin']) ,
            'marque': clear(row['marque']) ,
            'denomination_commerciale': clear(row['denomination_commerciale']) ,
            'couleur': clear(row['couleur']) ,
            'carrosserie': clear(row['carrosserie']),
            'categorie': clear(row['categorie']) ,
            'cylindres': clear(row['cylindres']) ,
            'energie': clear(row['energie']) ,
            'places': clear(row['places']) ,
            'poids': clear(row['poids']) ,
            'puissance': clear(row['puissance']) ,
            'type': clear(row['type']) ,
            'variante': clear(row['variante']) ,
            'version': clear(row['version']) ,
            'index' : index
        }
    return export_dict       

def DB_Create(): #Creates the output DB
    try:
        connection = sqlite3.connect(db_path)
        cursor = connection.cursor()
        cursor.execute("""CREATE TABLE vroom ( adresse_titulaire TEXT,nom TEXT,
        prenom TEXT,
        immatriculation TEXT,
        date_immatriculation TEXT,
        vin TEXT,
        marque TEXT,
        denomination_commerciale TEXT,
        couleur TEXT,
        carrosserie TEXT,
        categorie TEXT,
        cylindres TEXT,
        energie TEXT,
        places TEXT,
        poids TEXT,
        puissance TEXT,
        variante TEXT,
        version TEXT,
        type TEXT,
        ind INTEGER PRIMARY KEY AUTOINCREMENT
        );
        """)
        connection.commit()
        return cursor, connection
    except sqlite3.Error as identifier:
        error(identifier, connection)

def CheckExistance_DB():

    if os.path.exists(db_path): #Tests the DB existance
        try:
            connection = sqlite3.connect(db_path)
            cursor = connection.cursor()
        except sqlite3.Error as identifier:
            error(identifier, connection)
    else: #Creates One Otherwise
        cursor, connection = DB_Create()
    return cursor, connection

def CheckExistance_csv(path):
    if os.path.exists(path) and os.path.isfile(path) and path.split('.')[len(path.split('.')) - 1] == "csv":
        return True
    else:
        return False

def CheckExistance_log():
    if os.path.exists(log_path): #Tests log_path
        if os.path.isdir(log_path): #Tests if it's a file or repository
            file_name = (log_path + datetime.now().strftime("%Y-%m-%d") +'.log')
        else: #Otherwose, end task
            print("Error: log is a file")
            exit(1)
    else: #Otherwise, creates it
        try:
            os.mkdir(log_path)
            file_name = (log_path + datetime.now().strftime("%Y-%m-%d") +'.log')
        except OSError as error:
                print(error.__cause__)
                exit(1)

    return file_name

def DB_Update(cursor, dic):
    cursor.execute("UPDATE vroom SET adresse_titulaire = :adresse_titulaire, nom = :nom, prenom = :prenom, immatriculation = :immatriculation, date_immatriculation = :date_immatriculation, vin = :vin, marque = :marque, denomination_commerciale = :denomination_commerciale, couleur = :couleur, carrosserie = :carrosserie, categorie = :categorie, cylindres = :cylindres, energie = :energie, places = :places, poids = :poids, puissance = :puissance, variante = :variante, version = :version, type = :type WHERE ind = :index ", dic)
    return

def DB_Insert(cursor, dic):
    cursor.execute("insert into vroom (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindres, energie, places, poids, puissance, variante, version, type) values(:address_titulaire, :nom, :prenom, :immatriculation, :date_immatriculation, :vin, :marque, :denomination_commerciale, :couleur, :carrosserie, :categorie, :cylindres, :energie,:places, :poids, :puissance, :variante, :version, :type )", dic)


if __name__ =='__main__':
    csv_path = 'data.csv'
    cursor, connection = CheckExistance_DB()
    file_name = CheckExistance_log()
    CheckExistance_csv(csv_path)

    logging.basicConfig(format ='%(asctime)s : %(levelname)s : import.py : %(message)s', filename = file_name, level = logging.DEBUG)
    #Logging Module Config
    logging.info("Program Beginning")
    
    with open(csv_path) as csvfile: 
        logging.info("opening csv file")
        reader = csv.DictReader(csvfile, delimiter = ';')
        logging.info("delimiter chosen")

        for row in reader:
            cursor.execute("select * from vroom where immatriculation = ?", (clear(row['immatriculation'])))
            logging.info("Testing if %s exists in DB", row['immatriculation'])
            loop = 0

            for rowc in cursor.fetchall():
                if(len(row)>=20):
                logging.info("Current line is " + row.__str__())
                else:
                    loop = loop+1
                    test = rowc[19]
                    if(loop == 1):
                        logging.info("Present %d times", loop)
                        cursor.execute("""UPDATE vroom SET adresse_titulaire = :adresse_titulaire,
                        nom = :nom,
                        prenom = :prenom,
                        immatriculation = :immatriculation,
                        date_immatriculation = :date_immatriculation,
                        vin = :vin,
                        marque = :marque,
                        denomination_commerciale = :denomination_commerciale,
                        couleur = :couleur,
                        carrosserie = :carrosserie,
                        categorie = :categorie,
                        cylindres = :cylindres,
                        energie = :energie,
                        places = :places,
                        poids = :poids,
                        puissance = :puissance,
                        variante = :variante,
                        version = :version,
                        type = :type WHERE ind = :index """, inddicclear(row, test))
                        
                        logging.info("Updated values at index %d", test)
                    elif loop > 1:
                        logging.info("Present %d times", loop)
                        error("Error: An entry has multiple occurences", connection)
                    else:
                        logging.info("Data does not exist yet, exporting to DB")
                        cursor.execute("""insert into vroom (adresse_titulaire, nom,
                        prenom, immatriculation, date_immatriculation, vin, marque,
                        denomination_commerciale, couleur, carrosserie, categorie,
                        cylindres, energie, places, poids, puissance, variante,
                        version, type ) values(:adresse_titulaire, :nom, :prenom,
                        :immatriculation, :date_immatriculation, :vin, :marque,
                        :denomination_commerciale, :couleur, :carrosserie,
                        :categorie, :cylindree, :energie, :places, :poids,
                        :puissance, :variante , :version , :type )""", dicclear(row))
                        
                        logging.info("Written " + dicclear(row).__str__() + " into DB" )
                        logging.info("Written a total of " + loop.__str__() + " times")

    connection.commit()
    logging.debug("Modification sent")

    connection.close()
    logging.debug("DB connection closed")